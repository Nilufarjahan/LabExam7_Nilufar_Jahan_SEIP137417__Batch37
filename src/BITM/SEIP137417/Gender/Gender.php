<?php
namespace App\Gender;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Gender extends DB
{
    public $id="";
    public $name="";
    public $gender="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }

        if (array_key_exists('gender',$data))
        {
            $this->gender=$data['gender'];
        }


    }
    public function store(){

        $arrData  = array($this->name,$this->gender);

        $sql = "INSERT INTO gender( name, gender) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index(){
        echo "I am inside the index method of Gender class";
    }
}