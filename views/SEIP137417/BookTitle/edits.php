<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;

if(!isset( $_SESSION)) session_start();
echo Message::message();
$objBookTitle=new BookTitle();
$objBookTitle->setData($_GET);
$singleItem=$objBookTitle->view("obj");

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Atomic Project Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>
</head>
<body>

<div class="container" style="height:400px;background-color: cadetblue">
    <form class="form-horizontal" action="update.php" method="post">
        <input hidden name="id" value="<?php echo $singleItem->id; ?>">
        <h2>Edit Book Title </h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname">Book Name:</label>
            <div class="col-sm-10">
                <input type="text" name= "book_title" class="form-control" id="book" value="<?php echo $singleItem->book_title?> " required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="authorname">Author Name:</label>
            <div class="col-sm-10">
                <input type="text" name="author_name" class="form-control" id="author" value="<?php echo $singleItem->author_name?> " required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit"  class="btn btn-success">Save</button>
                <button type="submit"  class="btn btn-success">Save And ADD</button>
                <button type="reset"  class="btn btn-success">Reset</button>
                <button type="submit"  class="btn btn-success">Back To List</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

