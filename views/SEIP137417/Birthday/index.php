<?php

require_once("../../../vendor/autoload.php");
use App\Birthday\Birthday;


$birthdayObject = new Birthday();
$allData=$birthdayObject->index("obj");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Atomic Project Birthday</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Bordered Table</h2>
   <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Book Title</th>
        <th>Author Name</th>
          <th>Option</th>


      </tr>
    </thead>
    <tbody>
<tr>

<?php
      foreach($allData as $oneData)
      {
          ?>

<tr>

            <td><?php echo $oneData->id ?> </td>
             <td><?php echo $oneData->book_title ?></td>
             <td><?php echo $oneData->author_name ?></td>
            <?php  echo "<td> <a href=\"edits.php?id=$oneData->id\"class=\"btn btn-info\" role=\"button\">Edit</a>  ";?>
           <?php  echo " <a href=\"delete.php?id=$oneData->id\"class=\"btn btn-danger\" role=\"button\">Delete</a> </td> ";?>
</tr>
    <?php  }
    ?>
</tr>

    </tbody>
  </table>
</div>

</body>
</html>

