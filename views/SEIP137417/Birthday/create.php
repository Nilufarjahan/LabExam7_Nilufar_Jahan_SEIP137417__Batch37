<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
       <script>
        $(document).ready(function(){
            $("#msg").delay(2500).fadeOut("slow");
        });
    </script>
</head>
<body>

<div class="container" style="height:400px;background-color: cadetblue">
    <form class="form-horizontal" action="store.php" method="post">
        <h2>Add Birthday</h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookname">User Name:</label>
            <div class="col-sm-10">
                <input type="text" name= "name" class="form-control" id="book" placeholder="Enter  Name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="authorname">Birthday:</label>
            <div class="col-sm-10">
                <input type="date" name="birthday" class="form-control" id="author" placeholder="Enter Birthday" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit"  class="btn btn-success">Save</button>
                <button type="submit"  class="btn btn-success">Save And ADD</button>
                <button type="reset"  class="btn btn-success">Reset</button>
                <button type="submit"  class="btn btn-success">Back To List</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
